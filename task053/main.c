#include <stdio.h>

int main(int args, char *argv[]){
	int n;
	scanf("%d",&n);
	int a[n];
	readIntArray(a,n);
	popRepEl(a,&n);
	writeIntArray(a,n);
}

int popRepEl(int a[],int *n){
	int da[*n][2];
	int s_da=0;
	int s_a=*n;
	int b;
	int i;
	int k;
	for(i=0;i<s_a;i++){
		b=1;
		for(k=0;k<s_da;k++){
			if (a[i]==da[k][0]){
				da[k][1]++;
				b=0;
				break;
			}
		}
		if (b){
			da[s_da][0]=a[i];
			da[s_da][1]=1;
			s_da++;
		}
	}
	int max=0;
	int c;
	for(i=0;i<s_da;i++){
		if (max<=da[i][1]){
		max=da[i][1];
		c=da[i][0];
		}
	}
	i=0;
	for(k=0;k<s_a;k++){
		if (a[k]==c){
			*n=*n-1;
			continue;
		}
		a[i]=a[k];
		i++;
	}
	return *n;
}
int writeIntArray(int a[], int n){
	for (int i=0;i<n;i++)
		printf("%d ",a[i]);
	printf("\n");
}
int readIntArray(int a[], int n){
	for(int i=0;i<n;i++){
		scanf(" %d",&a[i]);
	}
	return 0;
}

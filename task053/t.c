#include <stdio.h>
#include <stdlib.h>

int tf(int *n){
	printf("1 %d\n",*n);
	*n=3;
	return 0;
}
int main(int args, char *argv[]){
	int n=5;
	tf(&n);
	printf("2 %d\n", n);

	int *p = NULL;
	int *p2=NULL;
	int *p3=NULL;
//	p=(int*) malloc(2);
//	printf("%d\n",p);
//	p2=(int*) malloc(2);
//	printf("%d\n",p2);
//	p3=(int*) malloc(2);
//	printf("%d\n",p3);
	for (int i=0;i<64;i++){
		p=(int*) malloc(i);
		*p=1;
		p2=(int*) malloc(i);
		*p2=1;
		p3=(int*) malloc(i);
		*p3=1;
		printf("%3d| %5d %5d : %10d %10d %10d - %10d\n",i,p3-p2,p2-p,p,p2,p3,p2+1 );
		free(p);
		free(p2);
		free(p3);
		p=NULL;
		p2=NULL;
		p3=NULL;
	}
	return 0;
}

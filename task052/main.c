#include <stdio.h>
int rightShift(float a[], int n, int m);

int main(int args, char argv[]){
	int n, k;
	printf("Enter the size of the array: ");
	scanf("%d",&n);
	float a[n];
	printf("Enter the array elements:\n");
	for(int i=0;i<n;i++){
		scanf(" %f",&a[i]);
	}
	printf("right shift: ");
	scanf("%d",&k);
	rightShift(a,n,k);
	for (int i=0;i<n;i++)
		printf("%.2f ", a[i]);
	printf("\n");
}

int rightShift(float a[], int n, int m){
	float swap[2];
	float s[m];
	int c=0;
	for(int i=0;i<m;i++)
		s[i]=a[i];
	for(int k=0;k<m;k++){
		swap[!c]=s[k];
		for (int i=m+k;i<n+m;i+=m){
			swap[c]=a[i%n];
			a[i%n]=swap[!c];
			c=!c;
		}
	}

}


